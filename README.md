# Internship on TCP congestion control optimisation 

* current status date 3/7/2018

A que network model has been made and initialized with service probability of que is Ps serving one packet at a time and arriving 
packets from two sources with no of packets coming from some probability.All initialization in model_que.py

The que network problem has been modeled as MDP problem with transition probability matrix Ptm. All the initialization is in Tmat.py

An MDP_Model.py has been made to get the optimal no of packets to send by one sender.

The solution of the system has been found in model_que.py

* Examples

The Examples folder contains the examples regarding MDP and reinforcement learning problem using Q Learning.
The Qlerning model and MDP_model has been made which can be used throughout.

The tesrting_models.py script is for testing the models which has been made.

* Current status at 11/7.20180

Current work is to solve que network problem when we do not know the network using partially observable MDP's.

Setup of NS2 simulator has been done and script file extract.py has been written which analyse the trace file and gives the require network output.

The pomdp problem has been modeled and formulated.
The script for the generation for transition matrix has been written for pomdp problem where we know the discritised belief state space and observation_space.







