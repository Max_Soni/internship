import numpy as np
import Tmat as Tmat
import MDP_Model as mdp
import qlearn_model as qlearn

def network_solution(que_length,p_service,num_arrivals,p_arrivals,discount_factor,max_ittr,method,epsilon=0.1,learning_rate=0):
    """
    Test_solution of que network:
    input:
    lenght of network que
    probability of service
    number of arrivals
    probability of each arrivals
    discount_factor
    maximum no of itteration
    epsilon value
    method: "mdp" or "reinforcement_learning"


    output:
    in case of mdp:
    number of itterations taken
    optimal policy
    value function
    other arrival states

    in case of Q learning:

    Returns updated Q matrix:
    """
    action_space,state_space,other_arrival_state,tpm,reward=Tmat.initialization(que_length,p_service,num_arrivals,p_arrivals)
    print(tpm)
    print('\n')
    print(reward)
    if method=="mdp":
        y=mdp.MDP_Model(state_space,action_space,tpm,reward,discount_factor)
        itterations,policy,value_function=y.solve_mdp("value_iteration",max_ittr,epsilon)
        return itterations,policy,value_function,other_arrival_state

    elif method=="rl":
        Q_function=np.zeros([1,len(state_space)*len(action_space)])
        Q_function=np.reshape(Q_function,(len(state_space),len(action_space)))
        y=qlearn.qlearn_model(state_space,action_space,reward,learning_rate,discount_factor,Q_function,tpm)
        Q_function=y._train_q(max_ittr,state_space[0])
        return Q_function,other_arrival_state,state_space,action_space

    else:
        return "specified method is not currect"
