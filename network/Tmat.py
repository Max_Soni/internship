import numpy as np 

def initialization(que_length,p_service,num_other_arrival,p_other_arrival):
    """ 
    input: 
    
    network_que_length
    service_probability
    arrival_probability, 
    max_number_arriving_packets
    probability_distribution of all arrivals 

    output:

    action_space
    state_space
    arrival_space
    transition_matrix
    reward_matrix
    
    """
    state_space=list()
    action_space=list()
    
    # added for more than one other arrival
    other_arrival_state=list()
    for i in range(num_other_arrival+1):
        other_arrival_state.append(i)

    # initializing state and action space
    for i in range(que_length+1):
        state_space.append(i)
        action_space.append(i)

    # initializing transition probability matrix
    tpm=np.zeros([1,len(action_space)*len(state_space)*len(state_space)])[0]
    tpm=np.reshape(tpm,(len(action_space),len(state_space),len(state_space)))

    # initializing probability array of service and arrival
    pa = p_other_arrival
    ps = [1 - p_service, p_service]
    
    reward=np.zeros([1,len(state_space)*len(action_space)])[0]
    reward=np.reshape(reward,(len(state_space),len(action_space)))

    for i in range(len(action_space)):
        for j in range(len(state_space)):
            temp_reward = 0
            for a in range(num_other_arrival+1):
                for s in range(0, 2):
                    k = min(max([j - s + a + i, 0]), que_length)
                    tpm[i, j, k] = tpm[i, j, k] + pa[a] * ps[s]
                    if j - s + a + i <= que_length:
                        temp_reward = temp_reward + pa[a] * ps[s] * i
                    else:
                        temp_reward = temp_reward + pa[a] * ps[s] * max([(que_length - (j - s + a)), 0])
            reward[j, i] = temp_reward
            
    return action_space,state_space,other_arrival_state,tpm,reward

# def main():
#     print("enter que length :")
#     que_length=int(input())
#     print("enter service probability :")
#     p_service=float(input())
#     print("enter entry probability :")
#     p_entry=float(input())
#     action_space,state_space,tpm,reward=initialization(que_length,p_service,p_entry)
#     print(tpm)


# if __name__=="__main__":
#     main()



        