import numpy as np 

class qlearn_model:
    """"
    This is the model to train Q value function using Q learning 
    Input:
    state_space
    action_space
    reward
    learning_rate
    discount_factor
    Q_function or policy
    transition_probability_matrix
    
    Output:
    trained Q matrix
    """
    def __init__(self,state_space,action_space,reward,learning_rate,discount_factor,Qfunction,tpm=[]):
        self.state_space=state_space
        self.action_space=action_space
        self.reward=reward
        self.tpm=tpm
        self.learning_rate=learning_rate
        self.discount_factor=discount_factor
        self.Qfunction=Qfunction
    
    def _available_action(self,current_state):
        if(len(self.reward[current_state,])==1):
            current_state_row=self.reward[current_state,][0]
        else:
            current_state_row=self.reward[current_state,]
        avl_action=list()
        for i in range(max(1,len(current_state_row))):
            if current_state_row[i]>=0:
                avl_action.append(i)
        return avl_action

    def _evaluate_policy(self,current_state,action,max_Qfunction):

        self.Qfunction[current_state,action]=self.Qfunction[current_state,action] \
        + self.learning_rate*(self.reward[current_state,action]+self.discount_factor*max_Qfunction-self.Qfunction[current_state,action])
        return self.Qfunction

    def _choose_state(self,action,current_state):
        if len(self.tpm)==0:
            next_state=np.random.choice(self.state_space,1)
        else:
            next_state=np.random.choice(self.state_space,1,p=self.tpm[action,current_state,])[0]
        return next_state

    def _choose_action(self,current_state):
        #available_act=self._available_action(current_state)
        next_action=int(np.random.choice(self.action_space,1))
        return next_action
        
    def _update_q(self,current_state,action,privious_state): 
        max_value=max(self.Qfunction[current_state,])
        self.Qfunction=self._evaluate_policy(privious_state,action,max_value)
        
        if(np.max(self.Qfunction)>0):
            return np.sum(self.Qfunction/np.max(self.Qfunction)*np.max(self.reward))
        else:
            return 0
        
    def _train_q(self,max_itteration,current_state=1):
        returned_score=list()
        itteration=0       
        for j in range(100):
            current_state=int(np.random.choice(self.state_space,1))
            for i in range(1,max_itteration):
                action=self._choose_action(current_state)
                next_state=self._choose_state(action,current_state)
                score=self._update_q(next_state,action,current_state)
                returned_score.append(score)
                current_state=next_state
                itteration +=1        
        return self.Qfunction