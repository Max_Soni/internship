import network_tests as test
import numpy as np

def main():
    print("Enter que length : ")
    que_length=int(input())
    print("Enter service probability : ")
    p_service=float(input())
    print("Enter the maximum no of other arrivals.")
    num_other_arrival=int(input())
    num_other_arrival=min(num_other_arrival,que_length)
    print("Enter the probability's of states")
    p_other_arrival=list()
    for i in range(num_other_arrival+1):
        p_other_arrival.append(float(input()))
    # initializing all the state and action space
    print("enter method to solve network:eg. mdp or rl")
    method=raw_input()
    print("enter discount factor : ")
    discount_factor=float(input())
    print("enter max itterations : ")
    max_iterations=int(input())
    if method=='rl':
        print("enter learning rate : ")
        learning_rate=float(input())
        Qfunction,other_arrival_state,state_space,action_space=test.network_solution(que_length,p_service,num_other_arrival,p_other_arrival,discount_factor,max_iterations,method,0,learning_rate)
        print("trained Q matrix : ")
        print(Qfunction)
        itteration = 0
        steps = []
        for j in range(len(state_space)):
            for i in range(len(action_space)):
                if Qfunction[j,i]==max(Qfunction[j,]):
                    next_step_index=i
            steps.append(next_step_index)
            itteration +=1

        print("==================================")
        print("Reinforcement learning solution : ")
        print("optimal policy : " + str(steps))

    elif method=='mdp':
        print("Enter the epsilon value : ")
        epsilon=float(input())
        itterations,policy,value_function,other_arrival_state=test.network_solution(que_length,p_service,num_other_arrival,p_other_arrival,discount_factor,max_iterations,method,epsilon)
        # solution
        print("=========================================================")
        print("solution : ")
        print("itterations : "+ str(itterations))
        print("optimal policy : " + str(policy))
        print("value_function : " + str(value_function))
        print("other arrival states are : " + str(other_arrival_state))

if __name__=="__main__":
    main()
