import matplotlib.pyplot as plt
import csv
import numpy as np
#Cwnd_file_original_updated
def extract(filename='out10.tr',cwnd_file='Cwnd_file_updated_QTCP_highly_congested',pomdp_file='../pomdp/update_pomdp.txt'):
    event=list()
    time=list()
    source_node=list()
    dest_node=list()
    packet_type=list()
    packet_size=list()
    flags=list()
    fid=list()
    source_addr=list()
    dest_addr=list()
    seq_num=list()
    pack_id=list()
    with open(filename,'r') as csvfile:
        plots=csv.reader(csvfile,delimiter=' ')
        for row in plots:
            event.append(str(row[0]))
            time.append(float(row[1]))
            source_node.append(int(row[2]))
            dest_node.append(int(row[3]))
            packet_type.append(str(row[4]))
            packet_size.append(str(row[5]))
            flags.append(str(row[6]))
            fid.append(int(row[7]))
            source_addr.append(str(row[8]))
            dest_addr.append(str(row[9]))
            seq_num.append(int(row[10]))
            pack_id.append(int(row[11]))
    
    cwnd_time_stamp=list()
    cwnd_size=list()
    with open(cwnd_file,'r') as csvfile:
        plot1=csv.reader(csvfile,delimiter=' ')
        for row in plot1:
            cwnd_time_stamp.append(float(row[0]))
            cwnd_size.append(float(row[1]))
    
    pomdp_time=list()
    pomdp_probability=list()
    
    with open(pomdp_file,'r') as csvfile:
        plot1=csv.reader(csvfile,delimiter=' ')
        for row in plot1:
            pomdp_time.append(float(row[1]))
            pomdp_probability.append(float(row[0]))
    return event,time,source_node,dest_node,packet_type,packet_size,flags,fid,source_addr,dest_addr,seq_num,pack_id,cwnd_time_stamp,cwnd_size,pomdp_time,pomdp_probability


class network_output:
    def __init__(self,event,time,source_node,dest_node,packet_type,packet_size,flags,fid,source_addr,dest_addr,seq_num,pack_id,cwnd_time_stamp,cwnd_size,pomdp_time,pomdp_probability):
        self.event=event
        self.time=time
        self.source_node=source_node
        self.dest_node=dest_node
        self.packet_type=packet_type
        self.packet_size=packet_size
        self.flags=flags
        self.fid=fid
        self.source_addr=source_addr
        self.dest_addr=dest_addr
        self.seq_num=seq_num
        self.packet_id=pack_id
        self.cwnd_time_stamp=cwnd_time_stamp
        self.cwnd_size=cwnd_size
        self.pomdp_time=pomdp_time
        self.pomdp_probability=pomdp_probability

    def _throughput(self,granularity):
        pass
    
    def _avgque(self):
        que=list()
        time_stamp=list()
        que_len=0
        for i in range(len(self.time)):
            if self.dest_node[i]==2:
                if self.event[i]=='+':
                    que_len +=1
                    que.append(que_len)
                    time_stamp.append(self.time[i])
                if self.event[i]=='-' or self.event[i]=='d':
                    que_len =que_len-1
                    que.append(que_len)                
                    time_stamp.append(self.time[i])
    
        plt.figure(3)
        plt.plot(time_stamp,que,label="que_len vs time")
        plt.xlabel('time in seconds')
        plt.ylabel('que_length')
        plt.title('time vs  que_length')
	plt.savefig('../results/average_que_length_QTCP_highly_congested.png')
        plt.show()    

    def pomdp_p(self):
        plt.figure(4)
        plt.plot(self.pomdp_time,self.pomdp_probability,marker='*',label="updated_state_prob vs time")
        plt.xlabel('time in seconds')
        plt.ylabel('updated belief_probability')
        plt.title('time vs  updated__belief_probability')
        #plt.show()    


    def _packet_loss(self):
        dropping_node=list()
        time_stamp=list()
        packet_drop_ratio=list()
        recv=0.0
        drop=0.0
        for i in range(len(self.time)):
            if self.event[i]=='r':
                recv +=1
            if self.event[i]=='d':
                dropping_node.append(self.dest_node[i])
                time_stamp.append(self.time[i])
                drop +=1
                packet_drop_ratio.append(drop)
        
        fraction=100/(drop+recv)
        packet_drop_ratio=np.multiply(fraction,packet_drop_ratio)
        plt.figure(2)
        plt.plot(time_stamp,packet_drop_ratio,marker='*',label="packet_drop_ratio_percentage vs time")
        plt.xlabel('time in seconds')
        plt.ylabel('dropped packet_ratio_percentage')
        plt.title('time vs  droppped_packet_percentage')
        plt.axis([0,40,0,9])
	plt.savefig('../results/packet_drop_QTCP_highly_congested.png')        
	plt.show()

        # return dropping_node,time_stamp,packet_drop_ratio    

    def _window_size(self):
        plt.figure(1)
        plt.plot(self.cwnd_time_stamp,self.cwnd_size,label="cwnd_size vs time")
        plt.xlabel('time in seconds')
        plt.ylabel('avg_cwnd_Size')
        plt.title('time vs avg_cwnd_Size')
	plt.savefig('../results/average_cwnd_QTCP_highly_congested.png')
        plt.show()

        # return self.cwnd_time_stamp,self.cwnd_size



def main():
    event,time,source_node,dest_node,packet_type,packet_size,flags,fid,source_addr,dest_addr,seq_num,pack_id,cwnd_time_stamp,cwnd_size,pomdp_time,pomdp_probability=extract()
    y=network_output(event,time,source_node,dest_node,packet_type,packet_size,flags,fid,source_addr,dest_addr,seq_num,pack_id,cwnd_time_stamp,cwnd_size,pomdp_time,pomdp_probability)
    # cwnd_time,cwnd_size
    # dropping_node,time_stamp,packet_id
    y._window_size()
    y._packet_loss()
    y._avgque()
    y.pomdp_p()

if __name__=="__main__":
    main()
