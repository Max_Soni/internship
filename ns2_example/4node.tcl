#creating simulation object 
set ns [new Simulator]

#defining different collours for data flow
$ns color 1 Blue 
$ns color 2 Red

#Open the nam trace file
set nf [open out.nam w]
$ns namtrace-all $nf

set nt [open out10.tr w]
$ns trace-all $nt

#Defining a finish procedure
proc finish {} {
    global ns nf nt
    $ns flush-trace
    #close the trace file
    close $nf
    close $nt
    #execute nam on the trace file
    # throughput_updated_original_
    exec perl throughput.pl out10.tr 2 0.1 >throughput_updated_test.txt
    exec nam out.nam &  
    exit 0
}

# creating nodes
set node0 [$ns node]
set node1 [$ns node]
set node2 [$ns node]
set node3 [$ns node]
set node4 [$ns node]
set node5 [$ns node]


# creating links between the nodes

# $ns duplex-link $node0 $node2 1Mb 10ms DropTail
# $ns duplex-link $node1 $node2 1Mb 10ms DropTail
# $ns duplex-link $node2 $node3 1Mb 10ms DropTail
# $ns duplex-link $node2 $node4 1Mb 10ms DropTail

$ns duplex-link $node0 $node2 2Mb 10ms DropTail
$ns duplex-link $node1 $node2 2Mb 10ms DropTail
$ns simplex-link $node2 $node3 0.3Mb 100ms DropTail
$ns simplex-link $node3 $node2 0.3Mb 100ms DropTail
$ns duplex-link $node3 $node4 0.5Mb 40ms DropTail
$ns duplex-link $node3 $node5 0.5Mb 30ms DropTail

$ns queue-limit $node2 $node3 20

# $ns duplex-link-op $node0 $node2 orient right-up
# $ns duplex-link-op $node1 $node2 orient right-down
# $ns duplex-link-op $node2 $node3 orient right-up
# $ns duplex-link-op $node2 $node4 orient right-down



#Tcp

set tcp [new Agent/TCP]
$ns attach-agent $node0 $tcp
set sink [new Agent/TCPSink/DelAck]
$ns attach-agent $node4 $sink
$ns connect $tcp $sink

$tcp set fid_ 1
$tcp set window_ 8000
$tcp set packetSize_ 552

# ftp

# set ftp [new Application/FTP]
# $ftp attach-agent $tcp
# $ftp set type_ FTP


# #udp 

# set udp [new Agent/UDP]
# $ns attach-agent $node1 $udp

# set null [new Agent/Null]
# $ns attach-agent $node3 $null

# $ns connect $udp $null

# #CBR 

# set cbr [new Application/Traffic/CBR]
# $cbr attach-agent $udp
# $cbr set type_ CBR 
# $cbr set packet_size_ 1000
# $cbr set rate_ 2Mb
# $cbr set random_ false 

#Setup a FTP over TCP connection
set ftp [new Application/FTP]
$ftp attach-agent $tcp
$ftp set type_ FTP
#Setup a UDP connection

set udp [new Agent/UDP]
$ns attach-agent $node1 $udp
set null [new Agent/Null]
$ns attach-agent $node5 $null
$ns connect $udp $null
$udp set fid_ 2

#Setup a CBR over UDP connection

set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type_ CBR
$cbr set packet_size_ 1000
$cbr set rate_ 0.01mb
$cbr set random_ false

$ns at 0.1 "$cbr  start"
$ns at 3  "$ftp  start"
$ns at 20   "$cbr  stop"
$ns at 40  "$ftp  stop"
$ns at 40.2 "finish"

proc plotWindow {tcpSource outfile} {
  global ns
  set now [$ns now]
  set cwnd [$tcpSource set cwnd_]

###Print TIME CWND   for  gnuplot to plot progressing on CWND   
  puts  $outfile  "$now $cwnd"

  $ns at [expr $now+0.1] "plotWindow $tcpSource  $outfile"
}
# Cwnd_file_original_updated
set outfile [open  "Cwnd_file_updated_test"  w]
$ns  at  0.0  "plotWindow $tcp  $outfile"

# Run simulation !!!!
$ns run


# Running simulation
