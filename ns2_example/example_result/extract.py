import matplotlib.pyplot as plt
import csv
import numpy as np

def extract(f1='highly_congested/Cwnd_file',f2='moderate_congestion/Cwnd_file',f3='no_congestion/Cwnd_file'):
    time1=list()
    time2=list()
    time3=list()
    p1=list()
    p2=list()
    p3=list()
    with open(f1,'r') as csvfile:
        plots=csv.reader(csvfile,delimiter=' ')
        for row in plots:
            time1.append(row[1])
            p1.append(row[0])
            
    with open(f2,'r') as csvfile:
        plot1=csv.reader(csvfile,delimiter=' ')
        for row in plot1:
            time2.append(row[1])
            p2.append(row[0])
    
    with open(f3,'r') as csvfile:
        plot1=csv.reader(csvfile,delimiter=' ')
        for row in plot1:
            time3.append(row[1])
            p3.append(row[0])
    
    time=np.max([len(time1),len(time2),len(time3)])

    for i in range(len(time1),time):
        p1.append(0)
    for i in range(len(time2),time):
        p2.append(0)
    for i in range(len(time3),time):
        p3.append(0)

    return time,p1,p2,p3

def main():
    time,p1,p2,p3=extract()
    time1=list()
    for i in range(time):
        time1.append(i)
    
    plt.plot(time1,p1,'r')
    plt.plot(time1,p2,'g')
    plt.plot(time1,p3,'b')
    plt.xlabel('time in seconds')
    plt.ylabel('probabilities')
    plt.title('time vs  p')
    plt.show()
       
        
if __name__=="__main__":
    main()