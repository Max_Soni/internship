import matplotlib.pyplot as plt
import csv
import numpy as np
time1=[]
time2=[]
throughput1=[]
throughput2=[]
with open('throughput_updated_test1.txt','r') as csvfile:
    plots=csv.reader(csvfile,delimiter=' ')
    for row in plots:
        time1.append(float(row[0]))
        throughput1.append(float(row[1])/8/1024)

with open('throughput_updated_test2.txt','r') as csvfile:
    plots=csv.reader(csvfile,delimiter=' ')
    for row in plots:
        time2.append(float(row[0]))
        throughput2.append(float(row[1])/8/1024)
        
throughput_1=np.sum(throughput1)
throughput_1=throughput_1/len(throughput1)

throughput_2=np.sum(throughput2)
throughput_2=throughput_2/len(throughput2)

print('overall_throughput_TCP:'+str(throughput_1))
print('overall_throughput_QTCP:'+str(throughput_2))
print('improvement:' +str(((throughput_2-throughput_1)/throughput_1)*100))

plt.plot(time1,throughput1,label='original_TCP')
plt.plot(time1,throughput2,label='Qlearned_TCP')
plt.xlabel('time in seconds')
plt.ylabel('average throughput in kBytes')
plt.title('time vs average throughput')
# for Qlearned and general TCP Congested case
plt.legend(loc='upper right')
plt.savefig('../results/throughput_test12.png')
plt.show()

