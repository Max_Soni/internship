import numpy as np 

def return_utility(v,T,u,reward,gamma):
    # action array 0 for contain and 1 to repair
    action_array=[0,1]
    for action in action_array:
        action_array[action]=reward[action] + gamma * np.sum(np.multiply(u,np.dot(v,T[:,:,action])))
    if(action_array[0]>=action_array[1]):
        k=0
    else:
        k=1

    return action_array[k], k 

def main():
    tot_states=4
    gamma=0.4
    ittr=0
    epslon=0.1

    # list containing data for each itteration
    graph_list=[]
    
    T=np.random.random((4,4,2))
    

    # transition matrix 
    T[0,0,0]=0.5
    T[0,0,1]=1
    
    T[0,1,0]=0.5
    T[0,1,1]=0
    
    T[0,2,0]=0
    T[0,2,1]=0
    
    T[0,3,0]=0
    T[0,3,1]=0
    
    T[1,0,0]=0
    T[1,0,1]=1
    
    T[1,1,0]=0.4
    T[1,1,1]=0
    
    T[1,2,0]=0.6
    T[1,2,1]=0
    
    T[1,3,0]=0.0
    T[1,3,1]=0
    
    T[2,0,0]=0.0
    T[2,0,1]=1
    
    T[2,1,0]=0.0
    T[2,1,1]=0
    
    T[2,2,0]=0.8
    T[2,2,1]=0
    
    T[2,3,0]=0.2
    T[2,3,1]=0
    
    T[3,0,0]=0.0
    T[3,0,1]=1
    
    T[3,1,0]=0.0
    T[3,1,1]=0
    
    T[3,2,0]=0
    T[3,2,1]=0
    
    T[3,3,0]=1
    T[3,3,1]=0
    
    # reward vector
    # reward to contain the machine and repair  
    r=np.array([[0.3,0.3,0.5,-5],[-1,-1,-1,-1]])
    
    #utility vector
    u=np.array([0,0,0,0])
    u1=np.array([0,0,0,0])
    m1=[]
    
    while True:
        delta=0.01
        u=u1.copy()
        u1 = []
        ittr +=1
        graph_list.append(u)

        for s in range(tot_states):
            reward=r[:,s]
            v=np.zeros((1,tot_states))
            v[0,s]=1.0
            temp = return_utility(v,T,u,reward,gamma)
            u1.append(temp[0])
            m = temp[1] 
            m1.append(m)
            delta=max(delta,np.abs(u1[s]-u[s]))

        u1 = np.array(u1)
        print(u1)
        if abs(delta) < epslon*(1-gamma)/gamma:
            print("=================== FINAL RESULT ==================")
            print("Iterations: " +str(ittr))
            print("Delta: " + str(delta))
            print("Gamma: " + str(gamma))
            print("Epsilon: " + str(epslon))
            print("===================================================")
            print(u[0:4])
            print(m1)
            print("===================================================")
            print("efficient policy")
            print(m1[-4:])
            print('final reward')
            print(r)
            break


if __name__=="__main__":
    main()

