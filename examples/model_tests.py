import MDP_Model as mdp 
import qlearn_model as qlearn
import numpy as np
def main():
# two state problem 
    # state_space1=[0,1]
    # action_space1=[0,1]
    # reward1=[0.6,-0.3,0.2,-0.1]
    # reward1=np.reshape(reward1,(len(state_space1),len(action_space1)))
    # tpm1=[0.2,0.8,0.4,0.6,1,0,1,0]
    # tpm1=np.reshape(tpm1,(len(action_space1),len(state_space1),len(state_space1)))
    # print("enter discount factor : ")
    # discount_factor=float(input())
    # print("enter max itterations : ")
    # max_iterations=int(input())
    # print("Enter the epsilon value : ")
    # epsilon=float(input())
    # print("Enter learnig rate : ")
    # learning_rate=float(input())    
    # Q=np.zeros([1,len(state_space1)*len(action_space1)])[0]
    # Q=np.reshape(Q,(len(state_space1),len(action_space1))) 
    # y=mdp.MDP_Model(state_space1,action_space1,tpm1,reward1,discount_factor)
    # itterations,policy,value_function=y.solve_mdp("value_iteration",max_iterations,epsilon)
    # print("MDP solution : ")
    # print(" iterations taken : "+ str(itterations))
    # print(" optimal policy : "+ str(policy))
    # print(" optimal utility value: "+ str(value_function))
    # z=qlearn.qlearn_model(state_space1,action_space1,reward1,learning_rate,discount_factor,Q,tpm1)
    # Q=z._train_q(max_iterations,state_space1[0])
    # print("learned Q matrix : "+ str(Q))

    # #results 
    # itteration = 0 
    # steps = []
    # for j in range(len(state_space1)):
    #     itteration +=1
    #     for i in range(len(action_space1)):
    #         if Q[j,i]==np.max(Q[j,]):
    #             next_step_index=i

    #     steps.append(next_step_index)
        
    # print("==================================")
    # print("Reinforcement learning solution : ")
    # print("itterations : "+ str(itteration))
    # print("optimal policy : " + str(steps))
    

#mdp problem
    state_space=[0,1,2,3]
    action_space=[0,1]
    
#transition matrix defination
    tpm=[0.6,0.2,0.15,0.05,
    0.1,0.4,0.3,0.2,
    0.1,0.1,0.3,0.5,
    0.1,0.1,0.1,0.7,
    1,0,0,0,
    1,0,0,0,
    1,0,0,0,
    1,0,0,0]
    tpm=np.reshape(tpm,(2,4,4))
    
    reward=[0.5,0.4,0.2,0.01,-0.6,-0.3,-0.1,-0.01]
    reward=np.reshape(reward,(2,4))
    reward=np.transpose(reward)
    print(reward)
    print("Enter discount factor : ")
    discount_factor=float(input())
    y=mdp.MDP_Model(state_space,action_space,tpm,reward,discount_factor)
    # print("Enter epsilon value : ")
    # epsilon=float(input())
    print("Enter max_iteration value : ")
    max_iterations=int(input())
    print("Enter learnig rate : ")
    learning_rate=float(input())    
    # iterations,policy,value_function=y.solve_mdp("value_iteration",max_iterations,epsilon)
    # print("itterations :"+ str(iterations))
    # print("policy_optimal : "+ str(policy))
    # print("optimal utility : " + str(value_function))
    
    # Q learning part 
    Q=np.zeros([1,len(state_space)*len(action_space)])
    Q=np.reshape(Q,(len(state_space),len(action_space)))
    z=qlearn.qlearn_model(state_space,action_space,reward,learning_rate,discount_factor,Q,tpm)
    Q=z._train_q(max_iterations,state_space[0])
    # print("learned Q matrix : "+ str(Q))
    #results 
    itteration = 0 
    steps = []
    for j in range(len(state_space)):
        itteration +=1
        for i in range(len(action_space)):
            if Q[j,i]==np.max(Q[j,]):
                next_step_index=i
        steps.append(next_step_index)
    print("==================================")
    print("Reinforcement learning solution : ")
    # print("itterations : "+ str(scores+itteration))
    print("optimal policy : " + str(steps))


# case:2 solving mdp.py using model
#     T = np.load("T.npy")
#     T=np.transpose(T)
#     state_space2=[0,1,2,3,4,5,6,7,8,9,10,11]
#     action_space2=[0,1,2,3]
#     reward=np.zeros([1,(len(state_space2)*len(action_space2))])[0]
#     reward=np.reshape(reward,(len(state_space2),len(action_space2)))
#     reward2=[-0.04, -0.04, -0.04,  +1.0, -0.04, 0.0, -0.04,-1.0,-0.04, -0.04, -0.04, -0.04]    
#     k=0
#     for i in range(len(state_space2)):
#         for j in range(len(action_space2)):
#             if j==i%4:
#                 reward[i,j]=reward2[k]
#                 k=k+1
    
#     print(reward)
#     y=mdp.MDP_Model(state_space2,action_space2,T,reward,discount_factor)
#     itterations,policy,value_function=y.solve_mdp("value_iteration",max_iterations,epsilon)
#     print('iterations taken : ',itterations)
#     print('optimal policy : ', policy )
#     print('optimal utility value: ', value_function)

# testing for reinforcement learning problem  
#     points_list = [(0,1), (1,5), (5,6), (5,4), (1,2), (2,3), (2,7)]
#     goal = 7
#     MATRIX_SIZE = 8

#     # create matrix x*y 
#     reward =np.ones([1,MATRIX_SIZE*MATRIX_SIZE])
#     reward=np.reshape(reward,(MATRIX_SIZE,MATRIX_SIZE)) 
#     reward *= -10
#     for point in points_list:
#         # print(point)
#         if point[1] == goal:
#             reward[point] = 1000
#         else:
#             reward[point] = 0

#         if point[0] == goal:
#             reward[point[::-1]] = 1000
#         else:
#             # reverse of point
#             reward[point[::-1]]= 0

#     # add goal point round trip
#     reward[goal,goal]= 1000
#     discount_factor=0.9
#     current_state = 0
#     steps = [current_state]
#     max_ittr=1500
#     itteration=0
#     state_space=list()
#     action_space=list()
#     for i in range(MATRIX_SIZE):
#         state_space.append(i)
#         action_space.append(i)
#     Q=np.zeros([1,len(state_space)*len(action_space)])
#     Q=np.reshape(Q,(len(state_space),len(action_space))) 
#     learning_rate=0.5
#     tpm=[]
#     y=qlearn.qlearn_model(state_space,action_space,reward,tpm,learning_rate,discount_factor,Q)
#     Q=y._train_q(max_ittr,state_space[0])
#     while current_state != 7:
#         itteration +=1
#         for i in range(len(state_space)):
#             if Q[current_state,i]==np.max(Q[current_state,]):
#                 next_step_index=i
                
#         steps.append(next_step_index)
#         current_state = next_step_index
#         if itteration>max_ittr:
#             break
#     print('itterations',itteration)
#     print("Most efficient path:")
#     print(steps)
    

    
   

if __name__=="__main__":
    main()