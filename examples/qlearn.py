import numpy as np
import pylab as plt

# map cell to cell, add circular cell to goal point
points_list = [(0,1), (1,5), (5,6), (5,4), (1,2), (2,3), (2,7)]


goal = 7

# import networkx as nx
# G=nx.Graph()
# G.add_edges_from(points_list)
# pos = nx.spring_layout(G)
# nx.draw_networkx_nodes(G,pos)
# nx.draw_networkx_edges(G,pos)
# nx.draw_networkx_labels(G,pos)
# plt.show()


# how many points in graph? x points
MATRIX_SIZE = 8

# create matrix x*y
R =np.ones([1,MATRIX_SIZE*MATRIX_SIZE])
R=np.reshape(R,(MATRIX_SIZE,MATRIX_SIZE)) 
R *= -1
print(R)

for point in points_list:
    # print(point)
    if point[1] == goal:
        R[point] = 100
    else:
        R[point] = 0

    if point[0] == goal:
        R[point[::-1]] = 100
    else:
        # reverse of point
        R[point[::-1]]= 0

# add goal point round trip
R[goal,goal]= 100

print(R)





Q=np.zeros([1,MATRIX_SIZE*MATRIX_SIZE])
Q=np.reshape(R,(MATRIX_SIZE,MATRIX_SIZE)) 

# learning parameter
gamma = 0.5

initial_state = 1

def available_actions(state):
    current_state_row = R[state,]
    av_act=list()
    for i in range(len(current_state_row)):
        if current_state_row[i]>=0:
            av_act.append(i)
    return av_act

available_act=available_actions(initial_state)

def sample_next_action(available_actions_range):
    next_action = int(np.random.choice(available_act,1))
    return next_action


action = sample_next_action(available_act)

def update(current_state, action, gamma):
    for i in range(MATRIX_SIZE):
        if Q[i,action]==np.max(Q[:,action]):
            max_index=i
    
  
    max_value = Q[action,max_index]
  
    Q[current_state, action] = R[current_state, action] + \
    gamma * max_value
    # print('max_value', R[current_state, action] + gamma * max_value)
  
    if (np.max(Q) > 0):
        return(np.sum(Q/np.max(Q)*100))
    else:
        return (0)
    

update(initial_state, action, gamma)

# Training
scores = []
for i in range(700):
    current_state = np.random.randint(0, int(Q.shape[0]))
    available_act = available_actions(current_state)
    action = sample_next_action(available_act)
    score = update(current_state,action,gamma)
    scores.append(score)
    # print ('Score:', str(score))
    
# print("Trained Q matrix:")
# print(Q/np.max(Q)*100)

# print("R matrix")
# print(R)



# Testing
current_state = 0
steps = [current_state]
max_ittr=10000
itteration=0
while current_state != 7:

    itteration +=1
    for i in range(MATRIX_SIZE):
        if Q[current_state,i]==np.max(Q[current_state,]):
            next_step_index=i
            
    steps.append(next_step_index)
    current_state = next_step_index
    if itteration>max_ittr:
        break

print('itterations',itteration)
print("Most efficient path:")
print(steps)

# plt.plot(scores)
# plt.show()