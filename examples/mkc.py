import numpy as np
import pylab as plt
from cvxopt import matrix

# import networkx as nx
# G=nx.Graph()
# G.add_edges_from(points_list)
# pos = nx.spring_layout(G)
# nx.draw_networkx_nodes(G,pos)
# nx.draw_networkx_edges(G,pos)
# nx.draw_networkx_labels(G,pos)
# plt.show()

r=[[2,2,2,2],[-1,-1,-1,-1]]
# r=[-1,1,0,-1,-1,-0.5,-0.3,0]
r=np.reshape(r,(4,2))
T=[0.5,0.5,0,0,
    0,0.4,0.6,
    0,0,0,0.8,0.2,
    0,0,0,1,
    1,0,0,0,
    1,0,0,0,
    1,0,0,0,
    1,0,0,0]   
#transition matrix defination
T=np.reshape(T,(2,4,4))

# T=matrix(T,(4,4))
T=np.transpose(T)
Q=np.zeros_like(r)
gamma=0.99

initial_state=0

def av_actions(states):
    current_state_row=r[states,:]
    av_act=np.where(current_state_row>=0)[0]
    return av_act

available_act = av_actions(initial_state)

def sample_next_action(T,curr_state,next_state):
    next_action=np.multiply(r[curr_state,:],T[curr_state,next_state,:])
    print(next_action)
    if(next_action[0]>=next_action[1]):
        m_act=0
    else:
        m_act=1

    return m_act

action=sample_next_action(T,initial_state,initial_state)

def update(current_state,action,gamma):
    max_index= np.where(Q[action,]==np.max(Q[action,1]))[0]
    if max_index.shape[0]>1:
        max_index=int(np.random.choice(max_index,size=1))
    else:
        max_index =int(max_index)

    max_value=Q[action,max_index]
     
    Q[current_state,action]=r[current_state,action]+gamma*max_value
    print('max_value', r[current_state, action] + gamma * max_value)

    if(np.max(Q)>0):
        return(np.sum(Q/np.max(Q)))
    else:
        return (0)
    

update(initial_state,action,gamma)

scores=[]
actions=[]
avact=[]
for i in range(52):
    current_state=np.random.randint(0,int(Q.shape[0]))
    available_act=av_actions(current_state)
    action=sample_next_action(T,current_state,current_state)
    actions.append(action)
    score=update(current_state,action,gamma)
    scores.append(score)
    print('Score:',str(score))

print("Trained Q matrix:")

print(Q/np.max(Q)*10)


print('action sequence')
print(actions)

# Testing
current_state=0
steps=[current_state]
m=[]
for i in range(16):
    next_step_index=np.where(Q[current_state,:]==np.max(Q[current_state,:]))[0]
    next_step_index=int(next_step_index)
    steps.append(next_step_index)
    current_state=next_step_index
    m.append(current_state)

# while current_state !=3:
#     next_step_index=np.where(Q[current_state,]==np.max(Q[current_state,]))[1]

#     if next_step_index.shape[0]>1:
#         next_step_index=int(np.random.choice(next_step_index,size=1))
#     else:
#         next_step_index=int(next_step_index)
    

print("Most efficient path:")
print(steps)

print('c')
print(m)