import numpy as np

class MDP_Model:
    """
    This class is for solving MDp problem.
    initialize mdp using
    state_space,action_space
    transition_probability_matrix
    reward
    discount factor

    for solution call
    solve_mdp(method,max_itterations,epsilon)
    possible mathods are:
    "value_iteration"
    "policy_iteration"

    return values:
    iterations taken
    optimal policy
    value_function of utilities of states

    """
    def __init__(self, state_space, action_space, tpm, reward, discount_factor):
        self.state_space = state_space
        self.action_space = action_space
        self.tpm = tpm
        self.reward = reward
        self.discount_factor = discount_factor

    def solve_mdp(self, method,max_iterations,epsilon):
        if method == "value_iteration":
            iterations,policy,value_function=self.solve_mdp_valueiteration(max_iterations,epsilon)
            return iterations-1,policy,value_function

        elif method == "policy_iteration":
            pass

    def evaluate_policy(self, policy, max_iterations):
        value_function_old = np.zeros([1, len(self.state_space)])[0]
        value_function = np.zeros([1, len(self.state_space)])[0]

        for iteration in range(max_iterations):
            value_function_old = value_function.copy()
            for state_index, state in enumerate(self.state_space):
                current_action = int(policy[state_index])
                current_reward = self.reward[state_index, current_action]
                value_function[state_index] = current_reward + self.discount_factor * \
                                              np.dot(value_function_old, self.tpm[current_action,state_index,])

        return value_function

    def _return_utility(self,reward, state, valuefunction_old):
        action_array=np.zeros([1,len(self.action_space)])[0]
        for action in range(len(self.action_space)):
            action_array[action] = reward[action] + self.discount_factor * np.dot(valuefunction_old, self.tpm[action,state,])
        max_action=0
        temp=action_array[0]
        for i in range(len(action_array)):
            if action_array[i]>temp:
                max_action=i
                temp=action_array[i]
            else:
                max_action=max_action
                temp=temp

        return action_array[max_action],max_action

    def solve_mdp_valueiteration(self,max_iterations,epsilon):
        iterations = 0
        valuefunction_old=np.zeros([1,len(self.state_space)])[0]
        valuefunction_new=np.zeros([1,len(self.state_space)])[0]
        policy=np.zeros([1,len(self.state_space)])[0]
        list_valuefunction=list()

        while True:
            iterations += 1
            valuefunction_old=valuefunction_new.copy()
            delta=0
            list_valuefunction.append(valuefunction_old)
            for state_index, state in enumerate(self.state_space):
                reward = self.reward[state_index,]
                temp = self._return_utility(reward,state_index,valuefunction_new)
                valuefunction_new[state_index] = temp[0]
                policy[state_index] = temp[1]
                delta = max(delta,np.abs(valuefunction_new[state_index]-valuefunction_old[state_index]))

            if iterations > max_iterations:
                break
            if delta < epsilon:
                break

        return iterations,policy,valuefunction_new
