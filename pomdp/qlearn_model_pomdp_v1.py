import numpy as np 

class monte_carlo_pomdp:
    """"
    This is the model to train Q value function using Q learning 
    Input:
    state_space
    action_space
    reward
    learning_rate
    discount_factor
    Q_function or policy
    transition_probability_matrix
    
    Output:
    learnt value function
    """
    def __init__(self,belief_state_space,actual_state_space,action_space,discount_factor,observation_pmf,policy,tpm=[]):
        self.belief_state_space = belief_state_space
        self.actual_state_space = actual_state_space
        self.action_space = action_space
        self.discount_factor = discount_factor
        self.observation_pmf = observation_pmf
        self.policy = policy
        self.tpm = tpm

    def _evaluate_policy(self, iterations, max_iterations):
        value_function = np.zeros([1,(len(self.belief_state_space))])[0]

        for belief_index, belief in enumerate(self.belief_state_space):
            slot_rewards = np.zeros([iterations, max_iterations])
            for iteration in range(iterations):
                learning_rate = 1.0 * iteration / (iteration + 1)
                current_state = np.random.choice([0, 1], 1, p = [1 - belief, belief])[0]
                current_belief = belief
                current_belief_index = belief_index
                reward = 0.0
                
                for slot in range(max_iterations):
                    current_action = int(self.policy[current_belief_index])
                    slot_reward, current_observation, next_state = self._simulator(current_action, current_state)
                    reward = reward + np.power(self.discount_factor, slot) * slot_reward
                    slot_rewards[iteration, slot] = slot_reward
                    current_state = next_state
                    next_belief = self._update_belief(current_action, current_observation, current_belief)
                    
                    next_belief, next_belief_index = self._quantize(next_belief)
                    
                    current_belief = next_belief
                    current_belief_index = next_belief_index
                    
                value_function[belief_index] = learning_rate * value_function[belief_index] + (1 - learning_rate) * reward

        return value_function

    def _quantize(self, belief):
        quantized_belief_index = int(np.round(belief * (len(self.belief_state_space) - 1)))
        quantized_belief = self.belief_state_space[quantized_belief_index]
        return quantized_belief, quantized_belief_index

    def _update_belief(self, action, o, belief):
        if action == 1:
            temp1 = ((1 - belief) * self.tpm[1,0,1] + belief * self.tpm[1,1,1]) * self.observation_pmf[1, o]
            temp2 = ((1 - belief) * self.tpm[1,0,0] + belief * self.tpm[1,1,0]) * self.observation_pmf[0, o]
            next_belief = temp1 / (temp1 + temp2)
        else:
            next_belief = (1 - belief) * self.tpm[0,0,1] + belief * self.tpm[0,1,1]

        return next_belief

    def _simulator(self,action, current_state):
        state = self._choose_state(action,current_state)
        observation, reward = self._calc_observation_reward(action, state)        
        return reward, observation, state     
    
    def _choose_state(self,action,initial_state):
        state = np.random.choice(self.actual_state_space,1,p = self.tpm[action,initial_state,].tolist())[0]
        return state
    
    def _calc_observation_reward(self, action, state):
        if action == 0:
            reward = 0
            
        if action == 1:
            if state == 0:
                reward = np.random.choice([0,1], 1, p = self.observation_pmf[0,:].tolist())[0]
            if state == 1:
                reward = np.random.choice([0,1], 1, p = self.observation_pmf[1,:].tolist())[0]
        return reward, reward
    