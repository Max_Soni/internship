from __future__ import print_function
import numpy as np
import scipy as sp
import scipy.stats
from scipy.stats import norm
import sys,os,time
class qlearn_model:
    """"
    This is the model to train Q value function using Q learning for 2 states pomdp
    Input:
    state_space
    action_space
    observations
    learning_rate
    discount_factor
    Q_function or policy
    transition_probability_matrix

    Output:
    trained Q matrix
    """
    def __init__(self,state_space,Q_state_space,belief_space,action_space,Qfunction,observation_table,learning_rate,discount_factor,discretisation_factor,state_tpm=[]):
        self.state_space=state_space
        self.Q_state_space=Q_state_space
        self.belief_space=belief_space
        self.action_space=action_space
        self.state_tpm=state_tpm
        self.learning_rate=learning_rate
        self.discount_factor=discount_factor
        self.Qfunction=Qfunction
        self.observation_table=observation_table
        # for faster algorithm initialize it once and then use it everywhere
        self.len_state_space=len(self.state_space)
        self.len_action_space=len(self.action_space)
        self.len_q_space=len(self.Q_state_space)
        self.len_belief_space=len(self.belief_space)
        self.discretisation_factor=discretisation_factor
    # newly added
    def generate_reward(self,current_belief_state,current_state,action):
        delta=0.01
        mean = self.observation_table[0,current_belief_state]
        deviation = self.observation_table[1,current_belief_state]
        observation=sp.random.normal(mean,deviation)
        if observation<=0:
            observation=delta
        if action==0:
            reward=0.1
        else:
            reward=1.0/observation
        if reward <=0:
            reward=delta
        return reward,observation,mean,deviation
    
    # changed
    def _evaluate_policy(self,current_state,current_belief_state,action,max_Qfunction,reward):
        self.Qfunction[current_belief_state,action]=self.Qfunction[current_belief_state,action] \
        + self.learning_rate*(reward+self.discount_factor*max_Qfunction-self.Qfunction[current_belief_state,action])
        return self.Qfunction

    # doubt
    def next_belief_state(self,current_belief_state,current_action):
        # for 2 state case only
        m = current_belief_state
        i = int(m%(self.len_belief_space))
        belief_vector=list()
        belief_vector.append(i)
        belief_vector=[belief_vector[i]*self.discretisation_factor for i in range(len(belief_vector))]
        j=1-np.sum(belief_vector)
        belief_vector.append(j)
        updated_belief_vector,reward_list,observation_list=self._update_p(current_action,belief_vector,current_belief_state)
        new_belief_vector=list()
        for belief in updated_belief_vector:
            belief_index=self._quantize_p(belief)
            new_belief_vector.append(belief_index)

        next_belief_state=int(new_belief_vector[0])
        # if next_belief_state not in self.Q_state_space:
            # next_belief_state=int(np.max(self.Q_state_space))

        return next_belief_state,reward_list,observation_list

    def _update_p(self,currrent_action,p_n_vec,current_belief_state):
        next_p=list()
        reward_list=list()
        observation_list=list()
        for j in range(self.len_state_space):
            temp=0
            reward,observation,mean,deviation=self.generate_reward(current_belief_state,j,currrent_action)
            reward_list.append(reward)
            observation_list.append(observation)
            for i in range(self.len_state_space):
                temp += p_n_vec[i]*self.state_tpm[currrent_action,i,j]*norm(mean,deviation).pdf(observation)
            next_p.append(temp)
        a=np.sum(next_p)
        next_p=np.multiply(1/(a),next_p)
        return next_p,reward_list,observation_list

    def _quantize_p(self,belief):
        belief_index=int(np.round(belief*(self.len_belief_space-1)))
        return belief_index

    def _choose_state(self,action,current_state):
        if len(self.state_tpm)==0:
            next_state=np.random.choice(self.state_space,1)
        else:
            next_state=np.random.choice(self.state_space,1,p=self.state_tpm[action,current_state])[0]
        return next_state

    def _choose_action(self):
        next_action=int(np.random.choice(self.action_space,1))
        return next_action
    # changed
    def _update_q(self,belief_state,action,privious_belief_state,current_state,reward_list,observation_list):
        max_value = np.max(self.Qfunction[belief_state,])
        self.Qfunction=self._evaluate_policy(current_state,privious_belief_state,action,max_value,reward_list[current_state])


    def _train_q(self,max_iteration,current_belief_state=1,current_state=0):
        for i in range(max_iteration):
            action=self._choose_action()
            next_state=self._choose_state(action,current_state)
            next_belief_state,reward_list,observation_list=self.next_belief_state(current_belief_state,action)
            self._update_q(next_belief_state,action,current_belief_state,current_state,reward_list,observation_list)
            current_belief_state=next_belief_state
            current_state=next_state
            if i%1000 == 0:
                a=i/1000
                sys.stdout.write("%d \r"%a)
                sys.stdout.flush()
        return self.Qfunction

