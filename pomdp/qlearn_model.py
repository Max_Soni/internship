import numpy as np
import scipy as sp
import scipy.stats
from scipy.stats import norm
class qlearn_model:
    """"
    This is the model to train Q value function using Q learning
    Input:
    state_space
    action_space
    reward
    learning_rate
    discount_factor
    Q_function or policy
    transition_probability_matrix

    Output:
    trained Q matrix
    """
    def __init__(self,state_space,Q_state_space,belief_space,action_space,learning_rate,discount_factor,Qfunction,observation_table,state_tpm=[]):
        self.state_space=state_space
        self.Q_state_space=Q_state_space
        self.belief_space=belief_space
        self.action_space=action_space
        self.state_tpm=state_tpm
        self.learning_rate=learning_rate
        self.discount_factor=discount_factor
        self.Qfunction=Qfunction
        self.observation_table=observation_table

    # newly added
    def generate_reward(self,current_state,action):
        mean = self.observation_table[0,current_state]
        deviation = self.observation_table[1,current_state]
        delta=0.01
        observation=sp.random.normal(mean,deviation)
        if observation<=0:
            observation=delta
        if action==0:
            reward=0.001
        else:
            reward=1.0/observation
        if reward <=0:
            reward=delta
        return reward,observation
    # changed
    def _evaluate_policy(self,current_state,current_belief_state,action,max_Qfunction,reward):
        self.Qfunction[current_belief_state,action]=self.Qfunction[current_belief_state,action] \
        + self.learning_rate*(reward+self.discount_factor*max_Qfunction-self.Qfunction[current_belief_state,action])
        return self.Qfunction

    # doubt
    def next_belief_state(self,current_belief_state,current_action,discretisation_factor):
        # for 4 state case only
        m = current_belief_state
        i = int(m/(len(self.belief_space))**2)
        m1 = int(m%(len(self.belief_space))**2)
        j = int(m1/(len(self.belief_space)**1))
        k = int(m1%(len(self.belief_space)**1))
        # k=m2 /(len(self.belief_space))
        # l=m2%(len(self.belief_space))
        belief_vector=list()
        belief_vector.append(i)
        belief_vector.append(j)
        belief_vector.append(k)
        belief_vector=[belief_vector[i]*discretisation_factor for i in range(len(belief_vector))]
        l=1-np.sum(belief_vector)
        l=round(l,3)
        belief_vector.append(l)
        updated_belief_vector,reward_list,observation_list=self._update_p(current_action,belief_vector)
        new_belief_vector=list()
        for belief in updated_belief_vector:
            belief_index=self._quantize_p(belief)
            new_belief_vector.append(belief_index)

        next_belief_state=int(new_belief_vector[2]+new_belief_vector[1]*len(self.belief_space)+new_belief_vector[0]*len(self.belief_space)**2)
        if next_belief_state not in self.Q_state_space:
            next_belief_state=int(np.max(self.Q_state_space))

        return next_belief_state,reward_list,observation_list

    def _update_p(self,currrent_action,p_n_vec):
        next_p=list()
        reward_list=list()
        observation_list=list()
        for j in range(len(p_n_vec)):
            temp=0
            reward,observation=self.generate_reward(j,currrent_action)
            reward_list.append(reward)
            observation_list.append(observation)
            for i in range(len(p_n_vec)):
                temp += p_n_vec[i]*self.state_tpm[currrent_action,i,j]*norm(self.observation_table[0,j],self.observation_table[1,j]).pdf(observation)
            next_p.append(temp)
        a=np.sum(next_p)
        next_p=np.multiply(1/(a),next_p)
        return next_p,reward_list,observation_list

    def _quantize_p(self,belief):
        belief_index=int(np.round(belief*(len(self.belief_space)-1)))
        return belief_index

    def _choose_state(self,action,current_state):
        if len(self.state_tpm)==0:
            next_state=np.random.choice(self.state_space,1)
        else:
            next_state=np.random.choice(self.state_space,1,p=self.state_tpm[action,current_state])[0]
        return next_state

    def _choose_action(self):
        next_action=int(np.random.choice(self.action_space,1))
        return next_action
    # changed
    def _update_q(self,belief_state,action,privious_belief_state,current_state,reward_list,observation_list):
        max_value = np.max(self.Qfunction[belief_state,])
        self.Qfunction=self._evaluate_policy(current_state,privious_belief_state,action,max_value,reward_list[current_state])


    def _train_q(self,max_iteration,rand_ittr,current_belief_state=0,current_state=0):
        a=rand_ittr
        for i in range(max_iteration):
            action=self._choose_action()
            next_state=self._choose_state(action,current_state)
            next_belief_state,reward_list,observation_list=self.next_belief_state(current_belief_state,action,0.05)
            self._update_q(next_belief_state,action,current_belief_state,current_state,reward_list,observation_list)
            current_belief_state=next_belief_state
            current_state=next_state
            if i%1000 == 0:
                print(i/1000)
        return self.Qfunction

