import numpy as np 
import math
import MDP_Model_1 as mdp
# import qlearn_model_pomdp_v1 as qlearn 
def compute_tpm(belief_state_space, action_space, actualstate_transition_matrix, observation_pmf, discretisation_factor):

    belief_transition_probability_matrix = np.zeros([1,(len(action_space)*len(belief_state_space)*len(belief_state_space))])[0]
    belief_transition_probability_matrix = np.reshape(belief_transition_probability_matrix,(len(action_space),len(belief_state_space),len(belief_state_space)))

    for action_index, action in enumerate(action_space):
        for belief_index, belief in enumerate(belief_state_space):
            if action==0:
                next_belief = (1 - belief) * actualstate_transition_matrix[0,0,1] + \
                              belief * actualstate_transition_matrix[0,1,1]

                index = int(np.round(next_belief/discretisation_factor))
                belief_transition_probability_matrix[action,belief_index,index] += 1
                
            elif action==1:
                for o in range(len(observation_pmf[0,])):
                    unconditional_observation_pmf = (1 - belief) * observation_pmf[0, o] + belief * observation_pmf[1, o]
                    temp1 = ((1 - belief) * actualstate_transition_matrix[1,0,1] + \
                            belief * actualstate_transition_matrix[1,1,1]) * observation_pmf[1, o]
                    temp2 = ((1 - belief) * actualstate_transition_matrix[1,0,0] + \
                            belief * actualstate_transition_matrix[1,1,0]) * observation_pmf[0, o]
                    next_belief = temp1 / (temp1 + temp2)
                    index = int(np.round(next_belief/discretisation_factor))
                    belief_transition_probability_matrix[action,belief_index,index] += unconditional_observation_pmf
            else:
                pass

    return belief_transition_probability_matrix

def compute_reward(belief_state_space,action_space,observation_pmf,actual_state_space):
    reward=np.zeros([1,(len(belief_state_space)*len(action_space))])[0]
    reward=np.reshape(reward,(len(belief_state_space),len(action_space)))
    
    # reward_qlearn=np.zeros([1,(len(action_space)*len(action_space))])
    reward_qlearn=[0,0.8,0,0.05]
    reward_qlearn=np.reshape(reward_qlearn,(len(actual_state_space),len(action_space)))

    for action in action_space:
        for belief_index,belief in enumerate(belief_state_space):
            if action==0:
                reward[belief_index,action]=0
            elif action==1:
                reward[belief_index,action]=(1-belief)*observation_pmf[0,1]+belief*observation_pmf[1,1]
            else:
                pass



    return reward,reward_qlearn

def main():
    discretisation_factor=0.05
    belief_state_space = np.zeros([1,int(1/discretisation_factor) + 1])[0]
    for i in range(len(belief_state_space)):
        belief_state_space[i]=belief_state_space[i] + i * discretisation_factor
        
    action_space=[0,1]

    actualstate_transition_matrix=[0.5,0.5,0.5,0.5,0.3,0.7,0.2,0.8]
    actualstate_transition_matrix=np.reshape(actualstate_transition_matrix,(len(action_space),2,2))

    # observations_pmf=[0.1,0.8,0.1,0.9,0.05,0.05]
    # observations_pmf=np.reshape(observations_pmf,(2,3))
    observations_pmf1=[0.2,0.8,0.8,0.2]
    observations_pmf1=np.reshape(observations_pmf1,(2,2))
    
    belief_transition_probability_matrix = compute_tpm(belief_state_space, action_space, actualstate_transition_matrix,
                                                       observations_pmf1, discretisation_factor)
    actual_state_space=[0,1]
    reward,reward_qlearn=compute_reward(belief_state_space,action_space,observations_pmf1,actual_state_space)
    
    discount_factor=0.99
    y=mdp.MDP_Model(belief_state_space,action_space,belief_transition_probability_matrix,reward,discount_factor)
    epsilon=0.001
    max_iterations=10000
    iterations,policy,value_function=y.solve_mdp("value_iteration",max_iterations,epsilon)
    # mdp method results :
    print("Results from value itteration : ")
    print("solution : ")
    print("itterations : "+ str(iterations))
    print("optimal policy : " + str(policy))
    print("value_function : " + str(value_function))
    # reinforcement learning formulation:
    # print("Enter the learning rate : ")
    
    # learning_rate=float(input())
    # Q_function=np.zeros([1,(len(actual_state_space)*len(action_space))])[0]
    # Q_function=np.reshape(Q_function,(len(actual_state_space),len(action_space)))
    
    # x=qlearn.qlearn_model_pomdp(actual_state_space,action_space,reward_qlearn,learning_rate,discount_factor,Q_function,actualstate_transition_matrix)
    # Q_function,q_itter=x._train_q(max_iterations)
    # print(Q_function)
    
    # Q_function1=np.zeros([1,len(belief_state_space)*len(action_space)])[0]
    # Q_function1=np.reshape(Q_function1,(len(belief_state_space),len(action_space)))
    # z=qlearn.monte_carlo_pomdp(belief_state_space,actual_state_space,action_space,discount_factor,observations_pmf1,policy,actualstate_transition_matrix)
    # iterations = 40
    # max_iterations = 300
    # Value_function=z._evaluate_policy(iterations,max_iterations)   
    


    # # qLEARNING rESULT
    # print("pomdp_monte_Carlo value_function: ")
    # print(Value_function)

if __name__=="__main__":
    main()
