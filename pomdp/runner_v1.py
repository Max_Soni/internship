import numpy as np
import math
import MDP_Model_1 as mdp
import qlearn_model_pomdp_v1 as qlearn
import qlearn_model_2states as q2learn
import matplotlib.pyplot as plt
def compute_tpm(belief_state_space, action_space, actualstate_transition_matrix, observation_pmf, discretisation_factor):

    belief_transition_probability_matrix = np.zeros([1,(len(action_space)*len(belief_state_space)*len(belief_state_space))])[0]
    belief_transition_probability_matrix = np.reshape(belief_transition_probability_matrix,(len(action_space),len(belief_state_space),len(belief_state_space)))

    for action_index, action in enumerate(action_space):
        for belief_index, belief in enumerate(belief_state_space):
            if action==0:
                next_belief = (1 - belief) * actualstate_transition_matrix[0,0,1] + \
                              belief * actualstate_transition_matrix[0,1,1]

                index = int(np.round(next_belief/discretisation_factor))
                belief_transition_probability_matrix[action,belief_index,index] += 1

            elif action==1:
                for o in range(len(observation_pmf[0,])):
                    temp1 = ((1 - belief) * actualstate_transition_matrix[1,0,1] + \
                            belief * actualstate_transition_matrix[1,1,1]) * observation_pmf[1, o]
                    temp2 = ((1 - belief) * actualstate_transition_matrix[1,0,0] + \
                            belief * actualstate_transition_matrix[1,1,0]) * observation_pmf[0, o]
                    unconditional_observation_pmf = temp1 + temp2
                    next_belief = temp1 / (temp1 + temp2)
                    index = int(np.round(next_belief/discretisation_factor))
                    belief_transition_probability_matrix[action, belief_index, index] += unconditional_observation_pmf
            else:
                pass

    return belief_transition_probability_matrix

def compute_reward(belief_state_space,action_space,observation_pmf,actual_state_space, tpm):
    reward=np.zeros([1,(len(belief_state_space)*len(action_space))])[0]
    reward=np.reshape(reward,(len(belief_state_space),len(action_space)))

    for action in action_space:
        for belief_index,belief in enumerate(belief_state_space):
            if action == 0:
                reward[belief_index,action] = 0.1
            elif action == 1:
                reward[belief_index,action] = ((1 - belief) * tpm[1,0,0] + belief * tpm[1, 1, 0]) * observation_pmf[0,1] + \
                                              ((1 - belief) * tpm[1,0,1] + belief * tpm[1, 1, 1]) * observation_pmf[1,1]
            else:
                pass
    return reward

def main():
    discretisation_factor = 0.05
    belief_state_space = np.zeros([1,int(1/discretisation_factor) + 1])[0]
    for i in range(len(belief_state_space)):
        belief_state_space[i] = belief_state_space[i] + i * discretisation_factor

    actual_state_space = [0,1]
    action_space = [0,1]

    actualstate_transition_matrix = [0.95,0.05,0.95,0.05,0.95,0.05,0.95,0.05]
    actualstate_transition_matrix = np.reshape(actualstate_transition_matrix,(len(action_space),2,2))

    # observations_pmf=[0.1,0.8,0.1,0.9,0.05,0.05]
    # observations_pmf=np.reshape(observations_pmf, (2,3))
    observations_pmf = [0.8, 0.2, 0.2, 0.8]
    observations_pmf = np.reshape(observations_pmf, (2,2))

    belief_transition_probability_matrix = compute_tpm(belief_state_space, action_space, actualstate_transition_matrix,
                                                       observations_pmf, discretisation_factor)
    reward = compute_reward(belief_state_space,action_space,observations_pmf,actual_state_space, actualstate_transition_matrix)
    discount_factor = 0.1
    y = mdp.MDP_Model(belief_state_space,action_space,belief_transition_probability_matrix,reward,discount_factor)
    epsilon = 0.001
    max_iterations = 1000
    iterations,policy,value_function = y.solve_mdp("value_iteration",max_iterations,epsilon)

    # mdp method results :
    print("Results from value iteration : ")
    print("solution : ")
    print("iterations : "+ str(iterations))
    print("optimal policy : " + str(policy))
    print("value_function : ")
    print value_function

    evaluated_value_function = y.evaluate_policy(policy,1000)
    print(evaluated_value_function)

    # New changes are added here

    Q_state_space = list()
    for i in range(len(belief_state_space)):
        Q_state_space.append(i)

    Qfunction=np.zeros([1,len(Q_state_space)*len(action_space)])[0]
    Qfunction=np.reshape(Qfunction,(len(Q_state_space),len(action_space)))
    learning_rate=0.99
    max_iteration=int(1e4)
    observation_table=np.zeros([1,len(belief_state_space)*2])[0]
    observation_table=np.reshape(observation_table,(2,len(belief_state_space)))


    # observation table
    std=np.std(reward)
    for i in range(len(belief_state_space)):
        if reward[i,1]!=0:
            observation_table[0,i] = 1/reward[i,1]
        else:
            observation_table[0,i]=0.01
        observation_table[1,i] = std

    x=q2learn.qlearn_model(actual_state_space,Q_state_space,belief_state_space,action_space,Qfunction,observation_table,learning_rate,discount_factor,discretisation_factor,actualstate_transition_matrix)
    Qfunction=x._train_q(max_iteration)
    f=open('2_states/Qfunction_1.1.txt','w')
    for i in range(len(Qfunction[:,0])):
        for j in range(len(Qfunction[0,:])):
            f.write(str(Qfunction[i,j]))
            f.write(' ')
        f.write('\n')
    f.close()

    policy_qlearn=list()
    value_function_qlearn=list()
    states=list()
    for i in range(len(Qfunction[:,0])):
        action=np.argmax(Qfunction[i,])
        policy_qlearn.append(action)
        value_function_qlearn.append(Qfunction[i,action])
        states.append(i)
    f=open('2_states/resQ_function1.1.txt','w')
    for i in range(len(value_function_qlearn)):
        f.write(str(policy_qlearn[i]))
        f.write(' ')
        f.write(str(value_function_qlearn[i]))
        f.write('\n')
    f.close()
    print("\n Qlearning_solution : ")
    print(policy_qlearn)
    print("Value_function_Qlearn : ")
    print(value_function_qlearn)

    plt.plot(states,policy_qlearn)
    plt.xlabel('Q_belief_state_index')
    plt.ylabel('optimal_action')
    plt.title('2_states qlearning')
    plt.savefig('2_states/graphs/test1.1_0.05_1e5.png')
    # plt.show()

    # reinforcement learning formulation:
    # z = qlearn.monte_carlo_pomdp(belief_state_space, actual_state_space, action_space, discount_factor,
    #                              observations_pmf, policy, actualstate_transition_matrix)
    # iterations = 1000
    # max_iterations = 200
    # value_function=z._evaluate_policy(iterations,max_iterations)

    # # qlearning result
    # print("pomdp_monte_Carlo value_function: ")
    # print(value_function)

if __name__=="__main__":
    main()
