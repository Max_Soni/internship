#include <stdlib.h>
#include <math.h>
#include <iostream>
using namespace std;

class nextp
{
    public:
    float belief;
    float **observation_pmf;
    float **actual_state_transition_matrix;
    float next_state_belief;
    
    float **initialize_observations()
    {
        observation_pmf=(float**)malloc(2*sizeof(float*));
        for(int i=0;i<2;i++)
        {
            observation_pmf[i]=(float*)malloc(2*sizeof(float));
        }
        observation_pmf[0][0]=0.1;
        observation_pmf[0][1]=0.9;
        observation_pmf[1][0]=0.9;
        observation_pmf[1][1]=0.1;

        return observation_pmf;
    }
    
    float **initialize_actual_trans_matrix()
    {
        actual_state_transition_matrix=(float**)malloc(2*sizeof(float*));
        for(int i=0;i<2;i++)
        {
            actual_state_transition_matrix[i]=(float*)malloc(2*sizeof(float));
        }
        actual_state_transition_matrix[0][0]=0.5;
        actual_state_transition_matrix[0][1]=0.5;
        actual_state_transition_matrix[1][0]=0.4;
        actual_state_transition_matrix[1][1]=0.6;
        return actual_state_transition_matrix;
    }

    float next_belief()
    {
        float temp1=0.0;
        float temp2=0.0;
        actual_state_transition_matrix=initialize_actual_trans_matrix();
        observation_pmf=initialize_observations();
        for(int i=0;i<2;i++)
        {
            float temp_obseavation_pmf=(1-belief)*observation_pmf[0][i]+belief*observation_pmf[1][i];
            temp1 +=((1-belief)*actual_state_transition_matrix[0][1]+belief*actual_state_transition_matrix[1][1])*observation_pmf[1][i];
            temp2 +=((1-belief)*actual_state_transition_matrix[0][0]+belief*actual_state_transition_matrix[1][0])*observation_pmf[0][i];
        }
        next_state_belief=temp1/(temp1+temp2);

        return next_state_belief;
    }
};



int main()
{
    
    float next_state_belief=0.25;
    nextp x;
    x.belief=next_state_belief;
    for(int i=0;i<4;i++)
    {
    next_state_belief=x.next_belief();
    x.belief=next_state_belief;
    cout << next_state_belief;
    cout <<"\n";
    }
    
    return 0;
}





