import numpy as np
import csv
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
Qfucntion=np.zeros([1,1331*4])[0]
Qfucntion=np.reshape(Qfucntion,(1331,4))
i=0
with open('Qfunction2.txt','r') as csvfile:
        plot1=csv.reader(csvfile,delimiter=' ')
        for row in plot1:
                Qfucntion[i,:]=row
                i=i+1
fix_index=5
x=[i for i in range(605,726)]
y=[0,1,2,3]
X,Y=np.meshgrid(x,y)
z=Qfucntion[X,Y]
plt.figure()
CS = plt.contour(X, Y, z, 6,)
plt.clabel(CS, fontsize=9, inline=1)
plt.xlabel('index_belief_index')
plt.ylabel('value_function index')
plt.axis([600,670,0,3])
plt.title('Contour plot of value function generated for belief index 0.5')
plt.show()
