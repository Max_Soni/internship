import numpy as np
import matplotlib.pyplot as plt
import qlearn_model as qlearn
import csv

def test(rand_itter):
    state_space=list()
    action_space=list()
    n = 3

    for i in range(n+1):
        state_space.append(i)
        action_space.append(i)

    discretisation_factor=0.1
    belief_state_space = np.zeros([1,int(1/discretisation_factor) + 1])[0]

    for i in range(len(belief_state_space)):
        belief_state_space[i] = i

    Q_state_space = list()
    for i in range((int(1.0/discretisation_factor)+1)**(n)):
        Q_state_space.append(i)

    Qfunction=np.zeros([1,len(Q_state_space)*len(action_space)])[0]
    Qfunction=np.reshape(Qfunction,(len(Q_state_space),len(action_space)))
    max_iteration=int(1e6)

    state_tpm=[0.9,0.04,0.06,0.0,0.5,0.3,0.1,0.1,0.5,0.2,0.2,0.1,0.5,0.2,0.2,0.1,\
    0.1,0.2,0.5,0.2,0.05,0.15,0.5,0.3,0.05,0.05,0.3,0.6,0.1,0.2,0.3,0.4,\
    0.05,0.15,0.4,0.4,0.05,0.05,0.3,0.6,0.01,0.09,0.2,0.7,0.1,0.2,0.3,0.4,\
    0.01,0.09,0.3,0.6,0.01,0.09,0.2,0.7,0.01,0.09,0.1,0.8,0.1,0.2,0.3,0.4]
    state_tpm=np.reshape(state_tpm,(len(action_space),len(state_space),len(state_space)))

    observation_table=np.zeros([1,len(state_space)*2])[0]
    observation_table=np.reshape(observation_table,(2,len(state_space)))
    p = 0.2
    sd = 1

    # observation table
    for i in range(n+1):
        observation_table[0,i] = p+i
        observation_table[1,i] = sd+i
    # model parameters
    learning_rate = 0.8
    discount_factor = 0.5
    y = qlearn.qlearn_model(state_space,Q_state_space,belief_state_space,action_space,learning_rate,discount_factor,Qfunction,observation_table,state_tpm)
    Q_function = y._train_q(max_iteration,rand_itter)
    f=open('Qfunction5_1.txt','w')
    for i in range(len(Q_function[:,0])):
        for j in range(len(Q_function[0,:])):
            f.write(str(Q_function[i,j]))
            f.write(' ')
        f.write('\n')
    f.close()

    return Q_function

def main():

    Q = test(1)
    
    # with open('resQ_function4.txt','r') as csvfile:
    #     plots=csv.reader(csvfile,delimiter=' ')
    #     for index,row in enumerate(plots):
    #         a=row[0]
    #         policy.append(a)


    policy=list()
    value_function=list()
    states=list()
    for i in range(len(Q[:,0])):
        action=np.argmax(Q[i,])
        policy.append(action)
        value_function.append(Q[i,action])
        states.append(i)
    f=open('resQ_function5_2.txt','w')
    
    for i in range(len(value_function)):
        f.write(str(policy[i]))
        f.write(' ')
        f.write(str(value_function[i]))
        f.write('\n')
    f.close()

    plt.plot(states,policy)
    plt.xlabel('Q_belief_state_index')
    plt.ylabel('optimal_action')
    plt.title('with changed Transition_Matrix')
    plt.savefig('Qlearn_test_res/test5_2_1e5_0.1.png')
    plt.show()


if __name__ == '__main__':
    main()
