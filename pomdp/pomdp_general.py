import numpy as np
import scipy as sp
import MDP_Model_1 as mdp
class general_pomdp:
    """
    This class is for solving general pomdp with n-hidden-states and n-actions
    
    input:
    state_space
    action_space
    reward
    belief_space
    observation_table
    state_transition_model
    
    used_functions:
    next_belief,next_belief_index=_update_p(current_state,current_action) 

    """
    def __init__(self,state_space,action_space,reward,belief_space,observation_table,state_transition_model):
        self.state_space=state_space
        self.action_space=action_space
        self.reward=reward
        self.belief_space=belief_space
        self.observation_table=observation_table
        self.state_transition_model=state_transition_model
    
    def _update_p(self,currrent_action,p_n_vec,observation_vector):
        next_p=list()
        for j in range(len(p_n_vec)):
            temp=0
            for i in range(len(p_n_vec)):
                temp +=p_n_vec[i]*self.state_transition_model[currrent_action,i,j]*sp.stats.norm(self.observation_table[j,0],self.observation_table[j,1]).pdf(observation_vector[j])
            next_p.append(temp)
        
        a=np.sum(next_p)
        next_p=np.multiply(1/a,next_p)
        return next_p
    
    def _quantize_p(self,belief):
        belief_index=int(np.round(belief*(len(self.belief_space)-1)))
        return belief_index        
    
    def _interpolate(self,dest_index,discretisation):
        px=list()
        py=list()
        v1=dest_index[0]*discretisation
        v2=dest_index[0]*discretisation
        x1=[v1-0.5,v1+0.5]
        y1=[v2-0.5,v2+0.5]
        delta=1.0/(len(self.belief_space))
        x_val=list()
        y_val=list()
        k=0
        for i in range(len(self.belief_space)+1):
            x_val.append(x1[0]+delta*i)
            y_val.append(y1[0]+delta*i)
        resx=0.0
        resy=0.0
        while(k!=len(x_val)):    
            for b1_index,b1 in enumerate(self.belief_space):
                temp1=1
                temp2=1
                temp3=1
                for b2_index,b2 in enumerate(self.belief_space):
                    if b1_index!=b2_index:
                        temp1*=(x1[k]-b2)
                        temp3*=(y1[k]-b2)
                        temp2*=(b1-b2)
                px.append(temp1/temp2)
                py.append(temp3/temp2)
            k=k+1
        


    def _belief_transition_model(self,new_belief_vector):
        belief_transition_model=np.zeros([1,(len(self.belief_space)*len(self.belief_space)*len(self.state_space))])[0]
        belief_transition_model=np.reshape(belief_transition_model,(len(self.belief_space),len(self.belief_space),len(self.state_space)))
        for b1_index,b1 in enumerate(self.belief_space):
            for s_index,s in enumerate(self.state_space):
                belief_index=self._quantize_p(new_belief_vector[s_index])
                belief_transition_model[b1_index,belief_index,s_index]+=new_belief_vector[s_index]
        
        return belief_transition_model

    
def main():
    
    pass


if __name__=="__main__":
    main()